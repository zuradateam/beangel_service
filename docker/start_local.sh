docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
export DOCKER_HOSTNAME=$(docker-machine ip)
echo "Docker hostname is: " ${DOCKER_HOSTNAME}
cd ..
mvn clean install docker:build
cd docker
docker-compose up -d