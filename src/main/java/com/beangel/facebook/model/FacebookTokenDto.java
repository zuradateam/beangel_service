package com.beangel.facebook.model;

import lombok.Data;

@Data
public class FacebookTokenDto {
    private String authToken;
}
