package com.beangel.facebook.model;

import lombok.Data;

@Data
public class FacebookLongLivedTokenDto {
    String access_token;
    String token_type;
    Long expires_in;
}
