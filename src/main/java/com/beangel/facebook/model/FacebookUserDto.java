package com.beangel.facebook.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Reference;

import java.util.List;

@Builder
@Data
@EqualsAndHashCode
public class FacebookUserDto {
    private String id;
    private String email;
    private String firstName;
    private String lastName;
    private PagedList<Reference> inAppFriends;
    private String accessToken;
    private String longFacebookAccessToken;
}
