package com.beangel.facebook.sevice;


import com.beangel.facebook.model.FacebookTokenDto;
import com.beangel.facebook.model.FacebookUserDto;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Reference;
import org.springframework.social.facebook.api.User;

public interface FacebookService {
    FacebookUserDto authorizeAndGetUserInfo(FacebookTokenDto token);
    Facebook connectToApi(FacebookTokenDto token);
    User signInToFacebookAndGetInfo(Facebook connection);
    PagedList<Reference> getAllInAppFriends(Facebook connection);

    FacebookUserDto getUserInfoUsingLongLivedToken(String name);
}
