package com.beangel.facebook.sevice;

import com.beangel.exception.BaseException;
import com.beangel.exception.ErrorCode;
import com.beangel.facebook.model.FacebookLongLivedTokenDto;
import com.beangel.facebook.model.FacebookTokenDto;
import com.beangel.facebook.model.FacebookUserDto;
import com.beangel.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.social.connect.Connection;
import org.springframework.social.facebook.api.*;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.util.List;

/**
 * Created by Connectis on 11.07.2017.
 */
@Slf4j
@Service
public class FacebookServiceImpl implements FacebookService {


    private static final String APP_ID = "1977610922495655";
    private static final String APP_SECRET = "1ec9854248711d2d7b491c4a85562a3a";
    private static final String GRAPH_API_URL = "https://graph.facebook.com/oauth/access_token";
    private static final String FB_EXCHANGE_TOKEN1 = "fb_exchange_token";
    private static final String FB_EXCHANGE_TOKEN = "fb_exchange_token";
    private static final String CLIENT_SECRET = "client_secret";
    private static final String GRANT_TYPE = "grant_type";

    private final FacebookConnectionFactory facebookConnectionFactory;

    @Autowired
    private UserService userService;

    @Autowired
    public FacebookServiceImpl(FacebookConnectionFactory facebookConnectionFactory) {
        this.facebookConnectionFactory = facebookConnectionFactory;
    }

    @Override
    public FacebookUserDto authorizeAndGetUserInfo(FacebookTokenDto token){
        Facebook connection = connectToApi(token);
        checkPermissions(connection);
        User userProfile = signInToFacebookAndGetInfo(connection);
        PagedList<Reference> friends = getAllInAppFriends(connection);
        String longLivedAccessToken = getLongLivedAccessToken(connection,token);
        String secret = userService.registerIfUserDoesntExist(userProfile, longLivedAccessToken);
        String accessToken = userService.oauthAuthenticate(userProfile.getEmail(),secret);
        return FacebookUserDto
                .builder()
                .id(userProfile.getId())
                .email(userProfile.getEmail())
                .firstName(userProfile.getFirstName())
                .lastName(userProfile.getLastName())
                .inAppFriends(friends)
                .accessToken(accessToken)
                .longFacebookAccessToken(longLivedAccessToken)
                .build();
    }

    private void checkPermissions(Facebook facebook) {
        List<Permission> permissions = facebook.userOperations().getUserPermissions();
        permissions.forEach(permission -> {
            if(permission.getName().equals("email") && !permission.isGranted()){
                throw new BaseException(ErrorCode.FACEBOOK_NO_EMAIL_PERMISSION);
            }
            if(permission.getName().equals("user_friends") && !permission.isGranted()){
                throw new BaseException(ErrorCode.FACEBOOK_NO_FRIENDS_LIST_PERMISSION);
            }
        });

    }


    @Override
    public Facebook connectToApi(FacebookTokenDto token) {
        log.info("token: " + token.getAuthToken());
        AccessGrant accessGrant = new AccessGrant(token.getAuthToken());
        try{
            Connection<Facebook> connection = facebookConnectionFactory.createConnection(accessGrant);
            return connection.getApi();
        }catch(Exception ex){
            throw new BaseException(ErrorCode.FACEBOOK_EXCEPTION);
        }

    }

    @Override
    public User signInToFacebookAndGetInfo(Facebook facebook) {
        String [] fields = { "id", "email",  "first_name", "last_name" };
        return facebook.fetchObject("me", User.class, fields);
    }

    @Override
    public PagedList<Reference> getAllInAppFriends(Facebook facebook) {
        return facebook.friendOperations().getFriends();
    }

    @Override
    public FacebookUserDto getUserInfoUsingLongLivedToken(String name) {
        String longLivedAccessToken = userService.getLongLivedToken(name);
        FacebookTokenDto facebookTokenDto = new FacebookTokenDto();
        facebookTokenDto.setAuthToken(longLivedAccessToken);
        return authorizeAndGetUserInfo(facebookTokenDto);

    }

    public String getLongLivedAccessToken(Facebook facebook, FacebookTokenDto facebookTokenDto){
        String url = GRAPH_API_URL + "?client_id=" + APP_ID + "&client_secret="+APP_SECRET +"& grant_type=fb_exchange_token& fb_exchange_token="+facebookTokenDto.getAuthToken();
        ResponseEntity<FacebookLongLivedTokenDto> exchange = facebook.restOperations()
                .exchange(url, HttpMethod.GET, HttpEntity.EMPTY, FacebookLongLivedTokenDto.class);
        FacebookLongLivedTokenDto response = exchange.getBody();
        log.info("RESPONSE: " + response);
        return response.getAccess_token();
    }
}
