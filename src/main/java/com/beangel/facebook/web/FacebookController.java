package com.beangel.facebook.web;

import com.beangel.facebook.model.FacebookTokenDto;
import com.beangel.facebook.model.FacebookUserDto;
import com.beangel.facebook.sevice.FacebookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
public class FacebookController {

    private final FacebookService facebookService;

    @Autowired
    public FacebookController(FacebookService facebookService) {
        this.facebookService = facebookService;
    }

    @PostMapping(value = "/auth")
    public ResponseEntity<FacebookUserDto> authorizeAndGetUserInfo(@RequestBody FacebookTokenDto token) {
        return ResponseEntity.ok(facebookService.authorizeAndGetUserInfo(token));
    }

    @GetMapping(value = "/facebook/profile")
    public ResponseEntity<FacebookUserDto> getUserInfo(OAuth2Authentication auth){
        log.info("principal: {}", auth);
        return ResponseEntity.ok(facebookService.getUserInfoUsingLongLivedToken(auth.getUserAuthentication().getName()));
    }


    // TODO update list
    //TODO send invitation
}
