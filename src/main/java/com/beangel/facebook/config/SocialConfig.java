package com.beangel.facebook.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;

@Configuration
@EnableSocial
public class SocialConfig {
    @Bean
    public FacebookConnectionFactory facebookConnectionFactory() {
        FacebookConnectionFactory facebookConnectionFactory = new FacebookConnectionFactory("1977610922495655", "1ec9854248711d2d7b491c4a85562a3a");
        facebookConnectionFactory.setScope("email, user_friends");
        return facebookConnectionFactory;
    }

}