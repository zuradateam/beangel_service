package com.beangel.config;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;
import static org.springframework.messaging.simp.SimpMessageType.*;

@Configuration
//@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER) //https://spring.io/guides/tutorials/spring-boot-oauth2/

//https://docs.spring.io/spring-security/site/docs/current/reference/html/websocket.html
public class WebSocketSecurityConfig extends AbstractSecurityWebSocketMessageBrokerConfigurer {

    @Override
    protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
        messages
                .simpTypeMatchers(CONNECT, HEARTBEAT, UNSUBSCRIBE, DISCONNECT).permitAll()
                .simpDestMatchers("/app/**").permitAll()
                .simpSubscribeDestMatchers("/topic/**").authenticated()
                .simpTypeMatchers(MESSAGE, SUBSCRIBE).denyAll()
                .anyMessage().denyAll();
    }


    @Override
    protected boolean sameOriginDisabled() {
        return true;
    }


}