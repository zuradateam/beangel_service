package com.beangel.config;

import com.beangel.chat.websocket.TopicSubscriptionInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.server.HandshakeHandler;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

@Configuration
@EnableWebSocketMessageBroker
//@Order(HIGHEST_PRECEDENCE + 50)

public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {
/*

    @Autowired
    HandshakeHandler handshakeManager;
*/

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic");
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        //registry.addEndpoint("/chat");
        //registry.addEndpoint("/chat").setHandshakeHandler(handshakeManager)
        registry.addEndpoint("/chat").withSockJS();
    }

    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        registration.setInterceptors(topicSubscriptionInterceptor());
    }


    @Bean
    public TopicSubscriptionInterceptor topicSubscriptionInterceptor() {
        return new TopicSubscriptionInterceptor();
    }

/*
    @Bean
    public HandshakeHandler handshakeManager() throws Exception {
        return new HandshakeManager();
    }*/
}
