package com.beangel.config;

import com.google.common.base.Predicates;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Slf4j
public class SwaggerConfig {


    @Bean
    public Docket api()  {
        log.info("Swagger initialization");

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(Predicates.or(RequestHandlerSelectors.basePackage("com.beangel.user.web"),RequestHandlerSelectors.basePackage("com.beangel.notification.web"),RequestHandlerSelectors.basePackage("com.beangel.facebook.web"),RequestHandlerSelectors.basePackage("com.beangel.chat.web")))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(new ApiInfo("user_service API", "Rest API Documentation", "", null, new Contact("", "", ""), null, null));

    }
}