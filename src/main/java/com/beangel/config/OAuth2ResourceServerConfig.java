package com.beangel.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

//@Order(ManagementServerProperties.ACCESS_OVERRIDE_ORDER)
@Configuration
@EnableResourceServer
public class OAuth2ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/auth","/test.html", "/hello", "/socket.html",
                        "/swagger-ui.html", "/swagger-resources/**", "/v2/api-docs/**", "/h2/**").permitAll().anyRequest().authenticated();
    }
}