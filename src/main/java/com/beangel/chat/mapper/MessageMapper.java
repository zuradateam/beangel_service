package com.beangel.chat.mapper;


import com.beangel.chat.dto.MessageDto;
import com.beangel.chat.dto.OutputMessageDto;
import com.beangel.chat.model.Message;
import com.beangel.user.model.UserDto;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;

public class MessageMapper {
    public static OutputMessageDto mapToMessageDto(Message message ){
        Function<Message, OutputMessageDto> userMapper = message1 -> {
            LocalDateTime time =Instant.ofEpochMilli(message1.getDateTime())
                    .atZone(ZoneId.systemDefault())
                    .toLocalDateTime();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

            return OutputMessageDto.builder().messageType(message1.getMessageType())
                    .from(message1.getFromUser())
                    .text(message1.getData())
                    .time(time.format(formatter))
                    .epoch(message1.getDateTime())
                    .build();
        };
        return userMapper.apply(message);
    };
}
