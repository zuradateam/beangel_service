package com.beangel.chat.dto;

import com.beangel.chat.model.MessageType;
import lombok.AllArgsConstructor;
import lombok.Data;


public interface MessageModel {

    String getText();
    MessageType getMessageType();
}
