package com.beangel.chat.dto;

import com.beangel.chat.model.MessageType;
import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class MessageDto implements MessageModel {
    private String from;
    private String text;
    private MessageType messageType;
}
