package com.beangel.chat.dto;

import com.beangel.chat.model.MessageType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;


@Data
@Builder
public class OutputMessageDto  implements MessageModel{
    private String time;
    private String from;
    private String text;
    private MessageType messageType;
    private Long epoch;

    public OutputMessageDto(String time, String from, String text, MessageType messageType) {
        this.time = time;
        this.from = from;
        this.text = text;
        this.messageType = messageType;
    }

    public OutputMessageDto(String time, String from, String text, MessageType messageType, Long epoch) {
        this.time = time;
        this.from = from;
        this.text = text;
        this.messageType = messageType;
        this.epoch = epoch;
    }
}
