package com.beangel.chat.websocket;

import com.beangel.chat.service.RoomService;
import com.beangel.user.repository.AngelRelationRepository;
import com.beangel.user.repository.UserRepository;
import com.beangel.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.stereotype.Component;

import java.security.Principal;


@Slf4j
public class TopicSubscriptionInterceptor extends ChannelInterceptorAdapter {

    private  RoomService roomService;

    @Autowired
    public void setRoomService(RoomService roomService){
        this.roomService = roomService;
    }

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(message);
        if (StompCommand.SUBSCRIBE.equals(headerAccessor.getCommand())) {
            Principal userPrincipal = headerAccessor.getUser();
            if (!validateSubscription(userPrincipal, headerAccessor.getDestination())) {
                throw new IllegalArgumentException("No permission for this topic");
            }
        }
        return message;
    }

    private boolean validateSubscription(Principal principal, String topicDestination) {
        if (principal == null) {
            // unauthenticated user
            return false;
        }
        log.info("Validate subscription for {} to topic {}", principal.getName(), topicDestination.substring(12));
        return roomService.checkIfUserIsAllowedInRoom(principal.getName(), topicDestination.substring(12));
    }


}