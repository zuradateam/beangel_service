package com.beangel.chat.websocket;

import com.beangel.chat.dto.MessageDto;
import com.beangel.chat.dto.OutputMessageDto;
import com.beangel.chat.model.Message;
import com.beangel.chat.repository.MessageRepository;
import com.beangel.chat.service.ChatService;
import com.beangel.chat.service.RoomService;
import com.beangel.exception.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.TextMessage;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;


@Controller
@Slf4j
public class ChatController {

    private final RoomService roomService;

    private final MessageRepository messageRepository;

    private final ChatService chatService;

    @Autowired
    public ChatController(RoomService roomService, MessageRepository messageRepository, ChatService chatService) {
        this.roomService = roomService;
        this.messageRepository = messageRepository;
        this.chatService = chatService;
    }

    //@PreAuthorize("hasRole('USER')")
    @MessageMapping("/room/{username}")
    @SendTo("/topic/room/{username}")
    public OutputMessageDto send(@DestinationVariable String username, MessageDto message, Principal principal) throws Exception {
        if(!roomService.checkIfUserIsAllowedInRoom(principal.getName(), username)){
            log.error("user is not authorized {}, {}",principal, username);
            throw new RuntimeException("user is not authorized: " + ErrorCode.USER_NOT_ALLOWED);
        }
        Long epoch = chatService.saveMessage(message,username,principal.getName());
        log.info("MessageDto {}", message);
        log.info("By user: " + principal.getName());
        String time = new SimpleDateFormat("HH:mm").format(new Date());
        return new OutputMessageDto(time, principal.getName(), message.getText(), message.getMessageType(), epoch);
    }

}
