/*
package com.beangel.chat.websocket;

import com.sun.security.auth.UserPrincipal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import java.security.Principal;
import java.util.List;
import java.util.Map;

@Slf4j
public class HandshakeManager extends DefaultHandshakeHandler {

    @Override
    protected Principal determineUser(ServerHttpRequest request,
                                      WebSocketHandler wsHandler, Map<String, Object> attributes) {

        String auth = request.getHeaders().getFirst("Authorization");
        Principal principal = request.getPrincipal();
        log.info("PRINC {}", principal);
        log.info("auth: "+ auth);
        return new UserPrincipal(principal.getName());
    }
}*/
