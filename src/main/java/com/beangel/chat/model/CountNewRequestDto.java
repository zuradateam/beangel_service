package com.beangel.chat.model;

import lombok.Data;

@Data
public class CountNewRequestDto {
    private String roomName;
    private Long epoch;
}
