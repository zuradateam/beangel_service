package com.beangel.chat.model;

import lombok.Data;


@Data
public class RoomRequestDto {

    private String roomName;
}
