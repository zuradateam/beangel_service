package com.beangel.chat.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "beangelMsg")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="message_id_seq")
    @SequenceGenerator(name="message_id_seq", sequenceName="message_id_seq", allocationSize=1)
    private Long id;
    private Long dateTime;
    private String fromUser;
    private String data;
    private String room;
    private MessageType messageType;
}
