package com.beangel.chat.model;

/**
 * Created by Connectis on 17.07.2017.
 */
public enum MessageType {
    MESSAGE,
    JOINING,
    LEAVING,
    PRAYER
}
