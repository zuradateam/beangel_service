package com.beangel.chat.web;

import com.beangel.chat.dto.OutputMessageDto;
import com.beangel.chat.model.CountNewRequestDto;
import com.beangel.chat.model.Message;
import com.beangel.chat.model.RoomRequestDto;
import com.beangel.chat.service.RoomService;
import com.beangel.user.model.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@Slf4j
public class MessageAndRoomController {

    private final RoomService roomService;

    @Autowired
    public MessageAndRoomController(RoomService roomService) {
        this.roomService = roomService;
    }

    //http://myfirstdomainadamzurada.com:6543/messages?page=0&size=2&sort=dateTime,desc
    @PostMapping(value = "/room/messages")
    public ResponseEntity<Page<OutputMessageDto>> getMessages(@RequestBody RoomRequestDto roomRequestDto, Principal auth, Pageable pageable){
        log.info("principal: {}", auth);
        return ResponseEntity.ok(roomService.getMessages(auth.getName(),roomRequestDto.getRoomName(),pageable));
    }

    @PostMapping(value = "/room/users")
    public ResponseEntity<List<UserDto>> getUsers(@RequestBody RoomRequestDto roomRequestDto, Principal auth){
        log.info("principal: {}", auth);
        return ResponseEntity.ok(roomService.getRoomUsers(auth.getName(),roomRequestDto.getRoomName()));
    }

    //TODO set timeZone
    @PostMapping(value = "/room/messages/countnew")
    public ResponseEntity<Integer> countNewMessages(@RequestBody CountNewRequestDto countNewRequestDto, Principal auth){
        log.info("principal: {}", auth);
        return ResponseEntity.ok(roomService.countNewMessagesToNotify(
                auth.getName(),
                countNewRequestDto.getRoomName(),
                countNewRequestDto.getEpoch())
        );
    }


}
