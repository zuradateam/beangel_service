package com.beangel.chat.service;

import com.beangel.chat.dto.MessageModel;
import com.beangel.chat.dto.OutputMessageDto;
import com.beangel.chat.model.Message;
import com.beangel.chat.model.MessageType;
import com.beangel.chat.repository.MessageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@Slf4j
public class ChatService {

    private  MessageRepository messageRepository;
    private final SimpMessagingTemplate simpMessagingTemplate;


    @Autowired
    public void setMessageRepository(MessageRepository messageRepository){
        this.messageRepository = messageRepository;
    }


    @Autowired
    public ChatService(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    public Long saveMessage(MessageModel message, String roomName, String userName){
            Message newMessage = new Message();
            newMessage.setRoom(roomName);
            newMessage.setData(message.getText());
            newMessage.setMessageType(message.getMessageType());
            newMessage.setFromUser(userName);
            newMessage.setDateTime(System.currentTimeMillis());
            Message savedMsg = messageRepository.save(newMessage);
            return savedMsg.getDateTime();
    }


    public void sendMessage(String from, String to, MessageType messageType){
        OutputMessageDto messageDto = generateMessageDto(from, messageType);
        saveMessage(messageDto,to,from);
        log.info("sending message to" +"/topic/room/"+to);
        simpMessagingTemplate.convertAndSend("/topic/room/"+to, generateMessageDto(from, messageType));

    }



    public OutputMessageDto generateMessageDto(String user, MessageType messageType){
        String time = new SimpleDateFormat("HH:mm").format(new Date());
        return new OutputMessageDto(time,user,"",messageType);
    }
}
