package com.beangel.chat.service;

import com.beangel.chat.dto.OutputMessageDto;
import com.beangel.chat.mapper.MessageMapper;
import com.beangel.chat.model.Message;
import com.beangel.chat.repository.MessageRepository;
import com.beangel.exception.BaseException;
import com.beangel.exception.ErrorCode;
import com.beangel.user.mapper.UserMapper;
import com.beangel.user.model.*;
import com.beangel.user.repository.AngelRelationRepository;
import com.beangel.user.repository.UserRepository;
import com.beangel.user.service.AngelService;
import com.beangel.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class RoomService {

    private  MessageRepository messageRepository;


    private final AngelService angelService;

    @Autowired
    public RoomService(AngelService angelService) {
        this.angelService = angelService;
    }
    /*
    @Autowired
    public void setUserService(UserService userService){
        this.userService = userService;
    }*/

    @Autowired
    public void setMessageRepository(MessageRepository messageRepository){
        this.messageRepository = messageRepository;
    }


    public Page<OutputMessageDto> getMessages(String user, String room, Pageable pageable ){
        if(checkIfUserIsAllowedInRoom(user,room )){
           return messageRepository.findByRoom(room, pageable).map(MessageMapper::mapToMessageDto);
        }else{
           throw new BaseException(ErrorCode.USER_NOT_ALLOWED);
        }
    }


    public boolean checkIfUserIsAllowedInRoom(String user, String room){
       return user.equals(room) || angelService.getMeAsAngelMyUsers(user).getMyUsers().stream().anyMatch(dbUser -> Objects.equals(dbUser.getUsername(), room));
    }


    public List<UserDto> getRoomUsers(String user, String room) {
        if(checkIfUserIsAllowedInRoom(user,room )){
                return angelService.getMyAngels(room).getMyAngels();
        }else{
            throw new BaseException(ErrorCode.USER_NOT_ALLOWED);
        }

    }

    public Integer countNewMessagesToNotify(String user, String room, Long timestamp) {
        if(checkIfUserIsAllowedInRoom(user,room )){
             return messageRepository.countByRoomAndDateTimeGreaterThan(room,timestamp);
        }else{
            throw new BaseException(ErrorCode.USER_NOT_ALLOWED);

        }
    }



}
