package com.beangel.chat.repository;


import com.beangel.chat.model.Message;
import com.beangel.chat.model.Room;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;


public interface MessageRepository extends JpaRepository<Message, Long> {
    //@RestResource(path = "byRoom", rel = "byRoom")
    Page<Message> findByRoom(@Param("room") String room, Pageable p);
    Integer countByRoomAndDateTimeGreaterThan(String room, Long epoch);
}
