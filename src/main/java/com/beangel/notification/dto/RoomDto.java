package com.beangel.notification.dto;

import lombok.Data;

@Data
public class RoomDto {
    private String username;
}
