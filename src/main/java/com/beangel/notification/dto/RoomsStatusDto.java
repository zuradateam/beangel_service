package com.beangel.notification.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
public class RoomsStatusDto {

    private List<RoomStatus> statuses;

    @Data
    @AllArgsConstructor
    public static class RoomStatus{
        private String roomName;
        private boolean seen;
        private Integer count;
    }
}
