package com.beangel.notification.web;

import com.beangel.notification.dto.RoomDto;
import com.beangel.notification.dto.RoomsStatusDto;
import com.beangel.notification.service.NotificationService;
import com.beangel.user.model.RevokeAngelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class NotificationController {

    private final NotificationService notificationService;

    @Autowired
    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }


    @GetMapping(value = "/notifications/updatelastseen")
    public ResponseEntity<String> updateMyRoom(OAuth2Authentication auth){
        notificationService.updateMyRoomLastSeen(auth.getUserAuthentication().getName());
        return ResponseEntity.ok("ok");
    }

    @PostMapping(value = "/notifications/updatelastseen")
    public ResponseEntity<String> updateMyUser(@RequestBody RoomDto roomDto, OAuth2Authentication auth){
        notificationService.updateMyUserLastSeen(auth.getUserAuthentication().getName(), roomDto.getUsername());
        return ResponseEntity.ok("ok");
    }


    @GetMapping(value = "/notifications/statuses")
    public ResponseEntity<RoomsStatusDto> getStatuses(OAuth2Authentication auth){
        return ResponseEntity.ok(notificationService.getRoomsStatuses(auth.getUserAuthentication().getName()));
    }

}
