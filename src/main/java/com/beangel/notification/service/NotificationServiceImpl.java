package com.beangel.notification.service;


import com.beangel.chat.service.RoomService;
import com.beangel.exception.BaseException;
import com.beangel.exception.ErrorCode;
import com.beangel.notification.dto.RoomsStatusDto;
import com.beangel.user.model.AngelRelation;
import com.beangel.user.model.User;
import com.beangel.user.repository.AngelRelationRepository;
import com.beangel.user.repository.UserRepository;
import com.beangel.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class NotificationServiceImpl implements NotificationService {


    private final
    UserRepository userRepository;

    private final
    AngelRelationRepository angelRelationRepository;

    private final
    RoomService roomService;

    private final
    UserService userService;

    @Autowired
    public NotificationServiceImpl(UserRepository userRepository, AngelRelationRepository angelRelationRepository, RoomService roomService, UserService userService) {
        this.userRepository = userRepository;
        this.angelRelationRepository = angelRelationRepository;
        this.roomService = roomService;
        this.userService = userService;
    }

    @Override
    public RoomsStatusDto getRoomsStatuses(String username) {
        List<RoomsStatusDto.RoomStatus> rooms = new ArrayList<>();
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isPresent()) {
            rooms.add(getMyRoomStatus(username, user.get()));
            getMyUsersStatuses(rooms,username);
        }else{
            throw new BaseException(ErrorCode.NO_SUCH_USER);
        }
        RoomsStatusDto roomsStatusDto = new RoomsStatusDto();
        roomsStatusDto.setStatuses(rooms);
        return roomsStatusDto;
    }

    @Override
    public Long updateMyRoomLastSeen(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        Long epoch = System.currentTimeMillis();
        if (user.isPresent()) {
            User user2update = user.get();
            user2update.getRoom().setLastSeen(epoch);
            userRepository.save(user2update);
        } else {
            throw new BaseException(ErrorCode.NO_SUCH_USER);
        }
        return epoch;
    }

    @Override
    public Long updateMyUserLastSeen(String angel, String user) {
        Long epoch = System.currentTimeMillis();
        if (userRepository.findByUsername(user).isPresent()) {
            Optional<AngelRelation> relation = angelRelationRepository.findByAngelIdAndUserId(angel, user);
            relation.ifPresent(t -> {
                relation.get().setLastSeen(epoch);
                angelRelationRepository.save(relation.get());
            });
        } else {
            throw new BaseException(ErrorCode.NO_SUCH_USER);
        }
        return epoch;
    }

    private RoomsStatusDto.RoomStatus getMyRoomStatus(String username, User user) {
        Long lastSeenEpochMyRoom = user.getRoom().getLastSeen();
        int count = roomService.countNewMessagesToNotify(username, username, lastSeenEpochMyRoom);
        boolean isSeen = count == 0;
        return new RoomsStatusDto.RoomStatus(username, isSeen, count);
    }

    private void getMyUsersStatuses(List<RoomsStatusDto.RoomStatus> rooms, String username) {
        angelRelationRepository
                .findAllByAngelId(username).forEach(angelRelation -> {

            Long lastSeenEpochMeAngel = angelRelation.getLastSeen();
            int count = roomService.countNewMessagesToNotify(username, angelRelation.getUserId(), lastSeenEpochMeAngel);
            boolean isSeen = count == 0;
            rooms.add(new RoomsStatusDto.RoomStatus(angelRelation.getUserId(), isSeen, count));
        });
    }
}
