package com.beangel.notification.service;


import com.beangel.notification.dto.RoomsStatusDto;

public interface NotificationService {
    RoomsStatusDto getRoomsStatuses(String username);
    Long updateMyRoomLastSeen(String username); //update myself on user, return epoch
    Long updateMyUserLastSeen(String angel, String user);//update my users room on angelrelation, return epoch
}
