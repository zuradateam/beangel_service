package com.beangel.user.mapper;

import com.beangel.user.model.User;
import com.beangel.user.model.UserDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

public class UserMapper {
    public static UserDto mapToUserDto(User user ){
        Function<User, UserDto> userMapper = user1 -> {
            UserDto userDto = new UserDto();
            userDto.setFacebookId(user1.getFacebookId());
            userDto.setFirstName(user1.getFirstName());
            userDto.setLastName(user1.getLastName());
            userDto.setUsername(user1.getUsername());
            return userDto;
        };
        return userMapper.apply(user);
    };
}
