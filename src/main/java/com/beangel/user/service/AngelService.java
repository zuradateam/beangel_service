package com.beangel.user.service;

import com.beangel.user.mapper.UserMapper;
import com.beangel.user.model.AngelRelation;
import com.beangel.user.model.MyAngelsDto;
import com.beangel.user.model.MyUsersDto;
import com.beangel.user.repository.AngelRelationRepository;
import com.beangel.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class AngelService {

    private final UserRepository userRepository;
    private final AngelRelationRepository angelRelationRepository;

    @Autowired
    public AngelService(UserRepository userRepository, AngelRelationRepository angelRelationRepository) {
        this.userRepository = userRepository;
        this.angelRelationRepository = angelRelationRepository;
    }

    public MyAngelsDto getMyAngels(String email) {
        return MyAngelsDto.builder().myAngels(userRepository.findByUsernameIn(
                angelRelationRepository
                        .findAllByUserId(email)
                        .stream()
                        .map(AngelRelation::getAngelId)
                        .collect(Collectors.toList()))
                .stream().map(UserMapper::mapToUserDto).collect(Collectors.toList()))
                .build();

    }

    public MyUsersDto getMeAsAngelMyUsers(String email) {
        return MyUsersDto.builder().myUsers(userRepository.findByUsernameIn(
                angelRelationRepository
                        .findAllByAngelId(email)
                        .stream()
                        .map(AngelRelation::getUserId)
                        .collect(Collectors.toList()))
                .stream().map(UserMapper::mapToUserDto).collect(Collectors.toList()))
                .build();
    }
}
