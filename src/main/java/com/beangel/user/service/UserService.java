package com.beangel.user.service;


import com.beangel.user.model.MyAngelsDto;
import com.beangel.user.model.MyUsersDto;
import com.beangel.user.model.UserDto;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

public interface UserService {

    default OAuth2AccessToken authenticate(String email, String password, AuthenticationProvider authenticationProvider){
        return authenticationProvider.authenticate(email, password);
    }

    String oauthAuthenticate(String username, String password);
    String registerIfUserDoesntExist(org.springframework.social.facebook.api.User user, String longLivedAccessToken);
    UserDto becomeAngelToUser(String angel, String email);
    void stopBeingAngelToUser(String angel,String email);
    String getLongLivedToken(String email);

    /*

    List<User> getMyAngels(String email, String token);
    List<User> getMeAsAngelMyUsers(String email, String token);
    void becomeAngelToUser(String email, String token);
    void stopBeingAngelToUser(String email, String token);
    */
}
