package com.beangel.user.service;


import org.springframework.security.oauth2.common.OAuth2AccessToken;

@FunctionalInterface
public interface AuthenticationProvider {
    OAuth2AccessToken authenticate(String email, String password);
}
