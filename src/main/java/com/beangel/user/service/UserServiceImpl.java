package com.beangel.user.service;


import com.beangel.chat.model.MessageType;
import com.beangel.chat.model.Room;
import com.beangel.chat.service.ChatService;
import com.beangel.exception.BaseException;
import com.beangel.exception.ErrorCode;
import com.beangel.user.mapper.UserMapper;
import com.beangel.user.model.*;
import com.beangel.user.model.User;
import com.beangel.user.repository.AngelRelationRepository;
import com.beangel.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final AngelRelationRepository angelRelationRepository;
    private final UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private ChatService chatService;
    @Value("${beangel.domain.url}")
    private String domainUrl;

    @Autowired
    public void setChatService(ChatService chatService){
        this.chatService = chatService;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder){
        this.passwordEncoder = passwordEncoder;
    }


    @Autowired
    public UserServiceImpl(AngelRelationRepository angelRelationRepository, UserRepository userRepository) {
        this.angelRelationRepository = angelRelationRepository;
        this.userRepository = userRepository;
    }


    @Override
    public String oauthAuthenticate(String username, String password) {
        return authenticate(username, password, (email, pass) -> {
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.set("grant_type", "password");
            map.set("username", email);
            map.set("password", pass);

            String encoding = Base64.getEncoder().encodeToString("beangel:thIIs%!C_$IsCla$$ified".getBytes());
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Basic " + encoding);
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            HttpEntity<MultiValueMap> entity = new HttpEntity<MultiValueMap>(map, headers);
            RestTemplate restTemplate = new RestTemplate();
            log.info(domainUrl + "/oauth/token");
            return restTemplate.postForObject(domainUrl + "/oauth/token", entity, OAuth2AccessToken.class);

        }).getValue();
    }


    @Override
    public String registerIfUserDoesntExist(org.springframework.social.facebook.api.User userProfile, String longLivedAccessToken) {
        String secret = UUID.randomUUID().toString();
        Optional<User> existingUser = userRepository.findByUsername(userProfile.getEmail());
        if (!existingUser.isPresent()) {
            Room room = new Room();
            room.setId(userProfile.getEmail());
            User user = new User(userProfile.getId(), userProfile.getEmail(), userProfile.getFirstName(), userProfile.getLastName(), passwordEncoder.encode(secret), true, secret, room, longLivedAccessToken);
            room.setUser(user);
            room.setLastSeen(System.currentTimeMillis());
            User newUser = userRepository.save(user);
            return newUser.getSecret();
        } else {
            existingUser.get().setLongLivedAccessToken(longLivedAccessToken);
            userRepository.save(existingUser.get());
            return existingUser.get().getSecret();
        }


    }


    @Override
    public UserDto becomeAngelToUser(String angel, String email) {
        Optional<User> user = userRepository.findByUsername(email);
        if(angel.equals(email)){
            throw new BaseException(ErrorCode.USER_SUBCRIPTION_TO_HIMSELF);
        }

        if (user.isPresent() && angelRelationRepository.findAllByAngelIdAndUserId(angel, email).size() == 0) {
            angelRelationRepository.save(new AngelRelation(angel, email, System.currentTimeMillis()));
            chatService.sendMessage(angel, email, MessageType.JOINING);
            return UserMapper.mapToUserDto(user.get());
        } else {
           throw new BaseException(ErrorCode.USER_IS_ALREADY_AS_ANGEL);
        }
    }

    @Override
    public void stopBeingAngelToUser(String angel, String email) {
        if(angel.equals(email)){
            throw new BaseException(ErrorCode.USER_SUBCRIPTION_TO_HIMSELF);
        }
        if (userRepository.findByUsername(email).isPresent()) {
            Optional<AngelRelation> relation = angelRelationRepository.findByAngelIdAndUserId(angel, email);
            relation.ifPresent(t -> {
                angelRelationRepository.delete(t);
                chatService.sendMessage(angel, email, MessageType.LEAVING);
            });
        }else{
            throw new BaseException(ErrorCode.NO_SUCH_USER);
        }
    }

    @Override
    public String getLongLivedToken(String email) {
        Optional<User> user = userRepository.findByUsername(email);
        if (user.isPresent()){
            return user.get().getLongLivedAccessToken();
        }else{
            throw new BaseException(ErrorCode.NO_SUCH_USER);
        }

    }
}
