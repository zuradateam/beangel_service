package com.beangel.user.web;

import com.beangel.chat.dto.MessageDto;
import com.beangel.chat.model.MessageType;
import com.beangel.chat.model.Room;
import com.beangel.chat.service.ChatService;
import com.beangel.user.model.*;
import com.beangel.user.repository.UserRepository;
import com.beangel.user.service.AngelService;
import com.beangel.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.TextMessage;

import java.security.Principal;

@RestController
@Slf4j
public class UserController {


    final
    BeangelApplicationTests bat;

    private final UserService userService;
    private final AngelService angelService;
    @Autowired
    public UserController(UserService userService, BeangelApplicationTests bat, AngelService angelService) {
        this.userService = userService;
        this.bat = bat;
        this.angelService = angelService;
    }
    /*
    @GetMapping(value = "/hello")
    public ResponseEntity<String> test() {
        //simpMessagingTemplate.setDefaultDestination("/topic");
       // simpMessagingTemplate.setUserDestinationPrefix("/app");

        //Room room1 = new Room();
        //room1.setId("adamzurada@gmail.com");
        //userRepository.save(new com.beangel.user.model.User("1234", "adamzurada@gmail.com", "first", "last", passwordEncoder.encode("secret"), true, "secret", room1));
       bat.givenRoomAndMessages_whenFindByRoom_thenReturnMessageList();
        return ResponseEntity.ok("ok");
    }*/

    @GetMapping(value = "/me")
    public ResponseEntity<String> secured(Principal auth){
        log.info("principal: {}", auth);
        return ResponseEntity.ok(auth.getName());
    }

    @GetMapping(value = "/myangels")
    public ResponseEntity<MyAngelsDto> getMyAngels(OAuth2Authentication auth){
        log.info("principal: {}", auth);
        return ResponseEntity.ok(angelService.getMyAngels(auth.getUserAuthentication().getName()));
    }

    @GetMapping(value = "/meangelmyusers")
    public ResponseEntity<MyUsersDto> getMyUsers(OAuth2Authentication auth){
        log.info("principal: {}", auth);
        return ResponseEntity.ok(angelService.getMeAsAngelMyUsers(auth.getUserAuthentication().getName()));
    }

    @PostMapping(value = "/become")
    public ResponseEntity<UserDto> becomeAngel(@RequestBody BecomeAngelDto becomeAngelDto, OAuth2Authentication auth){
        log.info("principal: {}", auth);
        log.info("topic: " + "/topic/room/"+becomeAngelDto.getUsername());
        return ResponseEntity.ok(userService.becomeAngelToUser(auth.getUserAuthentication().getName(), becomeAngelDto.getUsername()));
    }

    @PostMapping(value = "/stopbeingangel")
    public ResponseEntity<String> stopBeingAngel(@RequestBody RevokeAngelDto revokeAngelDto, OAuth2Authentication auth){
        log.info("principal: {}", auth);
        userService.stopBeingAngelToUser(auth.getUserAuthentication().getName(), revokeAngelDto.getUsername());
        return ResponseEntity.ok("ok");
    }

    @PostMapping(value = "/stopmyangel")
    public ResponseEntity<String> stopOneMyAngel(@RequestBody RevokeAngelDto revokeAngelDto, OAuth2Authentication auth){
        log.info("principal: {}", auth);
        userService.stopBeingAngelToUser(revokeAngelDto.getUsername(), auth.getUserAuthentication().getName());
        return ResponseEntity.ok("ok");
    }




}
