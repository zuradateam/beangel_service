package com.beangel.user.web;

import com.beangel.chat.model.Message;
import com.beangel.chat.model.Room;
import com.beangel.chat.repository.MessageRepository;
import com.beangel.chat.repository.RoomRepository;
import com.beangel.user.model.User;
import com.beangel.user.repository.AngelRelationRepository;
import com.beangel.user.repository.UserRepository;
import com.beangel.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;


@Slf4j
@Component
public class BeangelApplicationTests {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AngelRelationRepository angelRelationRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private RoomRepository roomRepository;

	@Autowired
	private MessageRepository messageRepository;


	public void givenRoomAndMessages_whenFindByRoom_thenReturnMessageList(){
		prepareData();
		Room room = roomRepository.findOne("username1@test.com");
		Message msg1 = new Message();
		msg1.setData("data");
		msg1.setDateTime(System.currentTimeMillis());
		msg1.setFromUser("uername1@test.com");
		msg1.setRoom(room.getId());
		messageRepository.save(msg1);
		Message msg2 = new Message();
		msg2.setData("data2");
		msg2.setDateTime(System.currentTimeMillis());
		msg2.setFromUser("uername2@test.com");
		msg2.setRoom(room.getId());
		messageRepository.save(msg2);
		Message msg3 = new Message();
		msg3.setData("data33");
		msg3.setDateTime(System.currentTimeMillis());
		msg3.setFromUser("uername3@test.com");
		msg3.setRoom(room.getId());
		messageRepository.save(msg3);

		//messageRepository.findByRoom(room, new );
	}

	private void prepareData(){
//		userRepository.deleteAll();
//		angelRelationRepository.deleteAll();

		Room room1 = new Room();
		room1.setId("username1@test.com");
		User user1 = new User("facebookId","username1@test.com","u1","zur","pass", true,"pass", room1 ,"");
		room1.setUser(user1);
		userRepository.save(user1);
		Room room2 = new Room();
		room2.setId("username2@test.com");
		User user2 = new User("facebookId","username2@test.com","u2","zur","pass", true,"pass", room2,"");
		room2.setUser(user2);
		userRepository.save(user2);
		Room room3 = new Room();
		room3.setId("username3@test.com");
		User user3 = new User("facebookId","username3@test.com","u3","zur","pass", true,"pass", room3,"");
		room3.setUser(user3);
		userRepository.save(user3);
		Room room4 = new Room();
		room4.setId("username4@test.com");
		User user4 = new User("facebookId","username4@test.com","u4","zur","pass", true,"pass", room4,"");
		room4.setUser(user4);
		userRepository.save(user4);
		Room room5 = new Room();
		room5.setId("username5@test.com");
		User user5 = new User("facebookId","username5@test.com","u5","zur","pass", true,"pass", room5,"");
		room5.setUser(user5);
		userRepository.save(user5);
		Room room6 = new Room();
		room6.setId("username6@test.com");
		User user6 = new User("facebookId","username6@test.com","u6","zur","pass", true,"pass", room6,"");
		room6.setUser(user6);
		userRepository.save(user6);
		Room room7 = new Room();
		room7.setId("username7@test.com");
		User user7 = new User("facebookId","username7@test.com","u6","zur","pass", true,"pass", room7,"");
		room7.setUser(user7);
		userRepository.save(user7);
	}
}
