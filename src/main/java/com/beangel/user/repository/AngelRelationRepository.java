package com.beangel.user.repository;


import com.beangel.user.model.AngelRelation;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AngelRelationRepository  extends CrudRepository<AngelRelation, Long> {

    //find who user is angel for
    List<AngelRelation> findAllByAngelId(String angelId);
    //find who is angel for a user
    List<AngelRelation> findAllByUserId(String userId);
    Optional<AngelRelation> findByAngelIdAndUserId(String angelId, String userId);
    List<AngelRelation> findAllByAngelIdAndUserId(String angelId, String userId);

}
