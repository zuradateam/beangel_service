package com.beangel.user.repository;


import com.beangel.user.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByUsername(String username);
    User findOneByUsername(String username);
    List<User> findByUsernameIn(List<String> usernames);
}
