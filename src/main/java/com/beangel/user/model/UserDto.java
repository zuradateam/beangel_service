package com.beangel.user.model;

import lombok.Data;

import javax.persistence.Id;

@Data
public class UserDto {
    private String facebookId;
    private String username;
    private String firstName;
    private String lastName;
}
