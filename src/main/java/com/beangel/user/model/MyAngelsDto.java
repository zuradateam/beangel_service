package com.beangel.user.model;


import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class MyAngelsDto {
    private List<UserDto> myAngels;

}
