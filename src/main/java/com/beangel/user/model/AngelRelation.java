package com.beangel.user.model;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class AngelRelation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="judgements_id_seq")
    @SequenceGenerator(name="judgements_id_seq", sequenceName="judgements_id_seq", allocationSize=1)
    private Long id;
    private String angelId;
    private String userId;
    private Long lastSeen;
    public AngelRelation(){

    }
    public AngelRelation(String angelId, String userId, Long lastSeen){
        this.angelId = angelId;
        this.userId = userId;
        this.lastSeen = lastSeen;
    }

}
