package com.beangel.user.model;

import lombok.Data;

@Data
public class BecomeAngelDto {
    private String username;
}
