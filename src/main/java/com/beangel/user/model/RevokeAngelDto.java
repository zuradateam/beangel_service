package com.beangel.user.model;

import lombok.Data;

/**
 * Created by Connectis on 13.07.2017.
 */
@Data
public class RevokeAngelDto {
    private String username;
}
