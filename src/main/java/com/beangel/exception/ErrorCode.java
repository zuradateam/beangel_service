package com.beangel.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorCode {
    USER_IS_ALREADY_AS_ANGEL(1000),
    NO_SUCH_USER(1001),
    USER_NOT_ALLOWED(1002),
    USER_SUBCRIPTION_TO_HIMSELF(1003),
    FACEBOOK_EXCEPTION(1004),
    FACEBOOK_NO_EMAIL_PERMISSION(1005),
    FACEBOOK_NO_FRIENDS_LIST_PERMISSION(1006);
    private final int code;


}
