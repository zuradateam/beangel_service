package com.beangel;

import com.beangel.chat.model.Message;
import com.beangel.chat.model.Room;
import com.beangel.chat.repository.MessageRepository;
import com.beangel.chat.repository.RoomRepository;
import com.beangel.user.model.AngelRelation;
import com.beangel.user.model.User;
import com.beangel.user.repository.AngelRelationRepository;
import com.beangel.user.repository.UserRepository;
import com.beangel.user.service.AngelService;
import com.beangel.user.service.UserService;
import junit.framework.TestCase;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.awt.print.Pageable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("local")
@Slf4j
public class BeangelApplicationTests  extends TestCase{

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AngelRelationRepository angelRelationRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private RoomRepository roomRepository;

	@Autowired
	private MessageRepository messageRepository;

	@Autowired
	private AngelService angelService;

	@Test
	public void contextLoads() {

	}

	@Test
	public void givenUsers_whenGetMyAngels_thanReturnList(){
		prepareData();

		angelRelationRepository.save(new AngelRelation("username2@test.com","username1@test.com", System.currentTimeMillis()));
		angelRelationRepository.save(new AngelRelation("username3@test.com","username1@test.com", System.currentTimeMillis()));
		angelRelationRepository.save(new AngelRelation("username4@test.com","username1@test.com", System.currentTimeMillis()));
		angelRelationRepository.save(new AngelRelation("username5@test.com","username1@test.com", System.currentTimeMillis()));
		angelRelationRepository.save(new AngelRelation("username6@test.com","username1@test.com", System.currentTimeMillis()));
		angelRelationRepository.save(new AngelRelation("username7@test.com","username1@test.com", System.currentTimeMillis()));
		List<AngelRelation> relations =  angelRelationRepository.findAllByUserId("username1@test.com");
		log.info("relations size {}", relations.size());
				relations.stream()
				.map(AngelRelation::getUserId)
				.collect(Collectors.toList())
				.forEach(s -> log.info("list: " + s));
		assertEquals(6, angelService.getMyAngels("username1@test.com").getMyAngels().size());

	}

	@Test
	public void clear(){
		messageRepository.deleteAll();
		roomRepository.deleteAll();
		userRepository.deleteAll();
		angelRelationRepository.deleteAll();
	}

	@Test
	public void givenUsers_whenGetMeAsAngelMyUsers_thanReturnList(){
		prepareData();

		angelRelationRepository.save(new AngelRelation("username1@test.com","username2@test.com", System.currentTimeMillis()));
		angelRelationRepository.save(new AngelRelation("username1@test.com","username3@test.com", System.currentTimeMillis()));
		angelRelationRepository.save(new AngelRelation("username1@test.com","username4@test.com", System.currentTimeMillis()));
		angelRelationRepository.save(new AngelRelation("username1@test.com","username5@test.com", System.currentTimeMillis()));
		angelRelationRepository.save(new AngelRelation("username1@test.com","username6@test.com", System.currentTimeMillis()));
		angelRelationRepository.save(new AngelRelation("username1@test.com","username7@test.com", System.currentTimeMillis()));
		angelRelationRepository.save(new AngelRelation("username2@test.com","username1@test.com", System.currentTimeMillis()));
		angelRelationRepository.save(new AngelRelation("username3@test.com","username1@test.com", System.currentTimeMillis()));

		List<AngelRelation> relations =  angelRelationRepository.findAllByAngelId("username1@test.com");
		log.info("relations size {}", relations.size());
		relations.stream()
				.map(AngelRelation::getUserId)
				.collect(Collectors.toList())
				.forEach(s -> log.info("list: " + s));

		angelService.getMeAsAngelMyUsers("username1@test.com");
		assertEquals(6, angelService.getMeAsAngelMyUsers("username1@test.com").getMyUsers().size());
		assertEquals(2, angelService.getMyAngels("username1@test.com").getMyAngels().size());

	}

	@Test
	public void givenRoomAndMessages_whenFindByRoom_thenReturnMessageList(){
		prepareData();
		Room room = roomRepository.findOne("username1@test.com");
		Message msg1 = new Message();
		msg1.setData("data");
		msg1.setDateTime(System.currentTimeMillis());
		msg1.setFromUser("uername1@test.com");
		msg1.setRoom(room.getId());
		messageRepository.save(msg1);
		Message msg2 = new Message();
		msg2.setData("data2");
		msg2.setDateTime(System.currentTimeMillis());
		msg2.setFromUser("uername2@test.com");
		msg2.setRoom(room.getId());
		messageRepository.save(msg2);
		Message msg3 = new Message();
		msg3.setData("data33");
		msg3.setDateTime(System.currentTimeMillis());
		msg3.setFromUser("uername3@test.com");
		msg3.setRoom(room.getId());
		messageRepository.save(msg3);
		org.springframework.data.domain.Pageable pageable =  new org.springframework.data.domain.Pageable() {
			@Override
			public int getPageNumber() {
				return 1;
			}

			@Override
			public int getPageSize() {
				return 2;
			}

			@Override
			public int getOffset() {
				return 0;
			}

			@Override
			public Sort getSort() {
				return new Sort(
						new Sort.Order(Sort.Direction.DESC, "dateTime")
				);
			}

			@Override
			public org.springframework.data.domain.Pageable next() {
				return null;
			}

			@Override
			public org.springframework.data.domain.Pageable previousOrFirst() {
				return null;
			}

			@Override
			public org.springframework.data.domain.Pageable first() {
				return null;
			}

			@Override
			public boolean hasPrevious() {
				return false;
			}
		};
		messageRepository.findByRoom(room.getId(), pageable)
		.forEach(message -> log.info("msg: {}",message));
	}

	private void prepareData(){
		messageRepository.deleteAll();
		roomRepository.deleteAll();
		userRepository.deleteAll();
		angelRelationRepository.deleteAll();

		Room room1 = new Room();
		room1.setId("username1@test.com");
		User user1 = new User("facebookId","username1@test.com","u1","zur","pass", true,"pass", room1,"");
		room1.setUser(user1);
		userRepository.save(user1);
		Room room2 = new Room();
		room2.setId("username2@test.com");
		User user2 = new User("facebookId","username2@test.com","u2","zur","pass", true,"pass", room2,"");
		room2.setUser(user2);
		userRepository.save(user2);
		Room room3 = new Room();
		room3.setId("username3@test.com");
		User user3 = new User("facebookId","username3@test.com","u3","zur","pass", true,"pass", room3,"");
		room3.setUser(user3);
		userRepository.save(user3);
		Room room4 = new Room();
		room4.setId("username4@test.com");
		User user4 = new User("facebookId","username4@test.com","u4","zur","pass", true,"pass", room4,"");
		room4.setUser(user4);
		userRepository.save(user4);
		Room room5 = new Room();
		room5.setId("username5@test.com");
		User user5 = new User("facebookId","username5@test.com","u5","zur","pass", true,"pass", room5,"");
		room5.setUser(user5);
		userRepository.save(user5);
		Room room6 = new Room();
		room6.setId("username6@test.com");
		User user6 = new User("facebookId","username6@test.com","u6","zur","pass", true,"pass", room6,"");
		room6.setUser(user6);
		userRepository.save(user6);
		Room room7 = new Room();
		room7.setId("username7@test.com");
		User user7 = new User("facebookId","username7@test.com","u6","zur","pass", true,"pass", room7,"");
		room7.setUser(user7);
		userRepository.save(user7);
	}
}
