FROM frolvlad/alpine-oraclejdk8:slim
VOLUME /tmp
ADD target/beangel_service-0.1.0.jar beangel_service.jar
RUN sh -c 'touch /beangel_service.jar'
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom  -jar /beangel_service.jar" ]